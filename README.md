## webdon-jekyll

Webdon for [Jekyll](https://jekyllrb.com), a skeleton to start from.

It depends on [webdon-sass](https://bitbucket.org/lilycode/webdon-sass) and [webdon-coffee](https://bitbucket.org/lilycode/webdon-coffee).

### Configuration

#### Social Links

You can add configuration options to have some links to your other online presence on social networks too by adding the following keys to `_config.yml`:

```yaml
twitter_name: YOUR_NAME_HERE
facebook_name: YOUR_NAME_HERE
```

These will be linked to `https://facebook.com/YOUR_NAME_HERE` or `https://twitter.com/YOUR_NAME_HERE` respectively.

#### Analytics

webdon-jekyll allows for some special keys to add analytics from a couple of different suppliers.

##### Google Analytics

Add your key from Google Analytics([link](http://www.google.com/analytics/)) into `_config.yml` like so:

```yaml
google_analytics_key: YOUR_KEY_HERE
```

##### Clicky

Add your key from Clicky ([affiliate link](http://clicky.com/100768440), [clean link](http://clicky.com)) into `_config.yml` like so:

```yaml
clicky_key: YOUR_KEY_HERE
```

