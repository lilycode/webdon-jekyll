require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

require 'jekyll-assets'
require 'jekyll-assets/compass'
require 'jekyll-assets/bourbon'
require 'jekyll-assets/neat'
require "jekyll-assets/font-awesome"
